/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Util.Impresora;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author DANIELA
 */
public class ShiftOr {

    // Función para buscar el patrón en el texto
    public static Paragraph searchPattern(char[] text, char[] pattern) throws DocumentException {
        LinkedList<Integer> posiciones = new LinkedList();
        int m = pattern.length;
        int n = text.length;

        Font font = new Font();
        Paragraph parrafo = new Paragraph();

        // Inicializar la máscara
        int mask = 1 << (m - 1);

        // Inicializar la tabla de bits
        int[] table = new int[256];

        // Inicializar la tabla con 0
        for (int i = 0; i < 256; i++) {
            table[i] = 0;
        }

        // Configurar los bits correspondientes a los caracteres en el patrón
        for (int i = 0; i < m; i++) {
            table[pattern[i]] |= 1 << i;
        }

        int state = 0;
        ArrayList<Chunk> car = new ArrayList<>();
        ArrayList<Chunk> subrayado = new ArrayList<>();
        // Proceso de búsqueda
        for (int i = 0; i < n; i++) {

            char currentChar = text[i];
            // Crear un Chunk para el carácter actual
            Chunk chunk = new Chunk(currentChar, font);
            car.add(chunk);

            state = (state << 1 | 1) & table[text[i]];
            // Verificar si se ha encontrado una ocurrencia
            if ((state & mask) != 0) {
                System.out.println("Entre");

                state = 0;
                posiciones.add(i - m + 1);
                subrayarChunkMejor((i - m + 1), i, car);
            }

            System.out.println("imprime car - > " + car.toString());

        }
        parrafo.addAll(car);
        parrafo.add("\n");
        String c = "El patron se repite " + posiciones.size() + " veces";
        parrafo.add(c);

        return parrafo;
    }
    
    public static void subrayarChunkMejor(int i, int tamanioPatron, ArrayList<Chunk> chunks) {
                
        for (int j = i; j <= tamanioPatron; j++) {
            chunks.get(j).setBackground(BaseColor.YELLOW);
        }
    }
    
    public static void subrayarChunk(int i, char[] texto, int tamaniop, Paragraph parrafo, ArrayList<Chunk> chunks) throws DocumentException {
        Font font = new Font();
        Chunk c = null;
        
        for (int j = 0; j < tamaniop; j++) {
            c = new Chunk((texto[i + j]), font);
            c.setBackground(BaseColor.YELLOW);
            chunks.set((i+j), c);
        }
    }
    
    //subaraya la subcadena especifica del texto, en el indice que se le pasa
    public static ArrayList subrayarP(int i, char[] texto, int tamaniop, Paragraph parrafo) throws DocumentException {
        Font font = new Font();
        Chunk c = null;
        ArrayList<Chunk> ch = new ArrayList<>();
        for (int j = 0; j < tamaniop; j++) {
            c = new Chunk((texto[i + j]), font);
            c.setBackground(BaseColor.YELLOW);
            ch.add(c);
        }
        return ch;
    }

    public static char[] deTextoAArreglo(String texto) {
        return null;
    }

    public static void main(String[] args) {
        String text = "ABABABACABA";
        String pattern = "ABA";
        char[] texto = text.toCharArray();
        char[] patron = pattern.toCharArray();
        //imprimir(searchPattern(texto, patron));

    }

    public static void imprimir(LinkedList<Integer> lista) {
        for (Integer elemento : lista) {
            System.out.print(elemento + "-");
        }
        System.out.println();
    }

}
