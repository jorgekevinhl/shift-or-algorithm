/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util;

import Modelo.ShiftOr;
import com.itextpdf.text.BaseColor;
//import com.itextpdf.layout.element.Text;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.LinkedList;



/**
 *
 * @author DANIELA
 */
public class Impresora<T> {

    private T desconocido;
    
    /**
     * Constructor vacío
     */
    public Impresora() {
    }

    public Impresora(T desconocido) {
        this.desconocido = desconocido;
    }

    public void imprimirConsola() {
        System.out.println(desconocido.toString());
    }
    
    /**
     * Pasa la información ingresada a un archivo PDF.
     * @param ruta donde se guardará el archivo.
     * @param posiciones donde se encuentran cada una de las coincidencias con el patrón
     * @throws FileNotFoundException
     * @throws DocumentException 
     */
    
    public void imprimirPDF(String ruta, Paragraph parrafo) throws FileNotFoundException, DocumentException {
        //1.Crear objeto PDF del documento
        Document documento = new Document();
        
        //2. Crear el archivo de almacenamiento--> PDF
        FileOutputStream ficheroPdf = new FileOutputStream(ruta);
        
        //3. Asignar la estructura del pdf al archivo físico:
        PdfWriter.getInstance(documento, ficheroPdf);
        documento.open();
        documento.add(parrafo);

        //5. Cerrar
        documento.close();
    }
    
    
    
}