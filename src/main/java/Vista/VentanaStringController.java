/**
 * Sample Skeleton for 'VentanaString.fxml' Controller Class
 */

package Vista;

import Modelo.ShiftOr;
import Util.Impresora;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class VentanaStringController {
    

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="cmdFileLoad"
    private Button cmdFileLoad; // Value injected by FXMLLoader

    @FXML // fx:id="cmdMatching"
    private Button cmdMatching; // Value injected by FXMLLoader

    @FXML // fx:id="cmdPDFOutput"
    private Button cmdPDFOutput; // Value injected by FXMLLoader

    @FXML // fx:id="txtOutput"
    private TextField txtOutput; // Value injected by FXMLLoader

    @FXML // fx:id="txtPatron"
    private TextField txtPatron; // Value injected by FXMLLoader


    private char[] texto = null;
    private char[] patron = null;
    File archivo = null;
    Paragraph parrafo = new Paragraph();

    @FXML
    void PDFOutput(ActionEvent event) {
        Stage nuevoStage = new Stage();
        try {
            Impresora imp = new Impresora();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Guardar PDF");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Archivos PDF", "*.pdf"));
            File file = fileChooser.showSaveDialog(nuevoStage);
            if (file != null) {
                imp.imprimirPDF(file.getAbsolutePath(), parrafo);
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML
    void fileLoad(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Selecciona un Archivo");
        ExtensionFilter extensionFilter = new ExtensionFilter("Archivos de texto (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extensionFilter);
        
        archivo = fileChooser.showOpenDialog(null); 

        if (archivo != null) {
            txtOutput.setText("Archivo seleccionado exitosamente"); 
        } else {
            txtOutput.setText("No se encontró el archivo");
        }
    }

    @FXML
    void matching(ActionEvent event) throws IOException, DocumentException {
        ShiftOr so = new ShiftOr();
        texto = array();
        patron = txtPatron.getText().toCharArray();
        parrafo = so.searchPattern(texto, patron);
        txtOutput.setText("Archivo procesado correctamente");
    }

    public String leer() throws FileNotFoundException, IOException {
        StringBuilder contenido = new StringBuilder();

        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                contenido.append(linea).append("\n");
            }
        } catch (IOException e) {
            System.err.print(e);
        }

        return contenido.toString();
    }

    
    public char[] array() throws IOException {
        return leer().toCharArray();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert cmdFileLoad != null : "fx:id=\"cmdFileLoad\" was not injected: check your FXML file 'VentanaString.fxml'.";
        assert cmdMatching != null : "fx:id=\"cmdMatching\" was not injected: check your FXML file 'VentanaString.fxml'.";
        assert cmdPDFOutput != null : "fx:id=\"cmdPDFOutput\" was not injected: check your FXML file 'VentanaString.fxml'.";
        assert txtOutput != null : "fx:id=\"txtOutput\" was not injected: check your FXML file 'VentanaString.fxml'.";
        assert txtPatron != null : "fx:id=\"txtPatron\" was not injected: check your FXML file 'VentanaString.fxml'.";
    }

}