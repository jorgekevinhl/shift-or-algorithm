module Vista {
    requires javafx.controls;
    requires javafx.fxml;
    requires itextpdf;
    opens Vista to javafx.fxml;
    exports Vista;
}
